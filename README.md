<!-- # KarmaTest

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 15.0.5.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page. -->


When you initialize a project using `ng new project-name` , it comes pre-built with the Karma testing framework.
#Run `ng test` which will test all the .spec.ts files in the project

To use Jest instead, follow these steps:
### i. Setup Jest

# Add packages
```
yarn add -D jest jest-preset-angular @types/jest
```
# Initilize jest.config.js file
```
yarn jest --init
```

PROMPTS:
Use code coverage-*Yes* => This will give coverage reports of how much of the code is touched by the tests
Red- little of the code touched
Yellow- average
Green- enough

Use typescript for the configuration file => *No*

# Update `package.json` with the following
```json
  ...
  "dependencies": {
    "test": "jest",
    "test:watch": "jest --watch",
    "test:ci": "jest --runInBand"
  ...
  }
  ...
```
# Update `./jest.config.js`

```
module.exports = {
  ...
  preset: "jest-preset-angular",
  setupFilesAfterEnv: ["<rootDir>/setup-jest.ts"],
  globalSetup: "jest-preset-angular/global-setup",
  ...
};

```

# create `./setup-jest.ts` file at the root of the project(where package.json is) and add the following line
```ts
import 'jest-preset-angular/setup-jest';

```
### ii. Remove Karma and Jasmine
# Delete `src/test.ts` and `./karma.conf.js` (if you have them)

# Update `angular.json` by deleting `"test": {...}` 

# Overwrite ( replace existing content )`tsconfig.spec.json` with the following

```json
/* To learn more about this file see: https://angular.io/config/tsconfig. */
{
  "extends": "./tsconfig.json",
  "compilerOptions": {
    "outDir": "./out-tsc/spec",
    "module": "CommonJs",
    "types": ["jest"]
  },
  "include": ["src/**/*.spec.ts", "src/**/*.d.ts"]
}
```
# Then run tests with 
```sh
yarn test
```
All  .spec.ts files in the workspace will be tested

# Or to use watch mode
```sh
yarn test:watch
```

By default, Jest will run tests against all changed files since the last commit, but watch mode also exposes many other options.

-a is equivalent to running jest --watchAll, which means it runs all tests, not just those since last commit.

-f is useful for focusing only on broken tests

-p is a way to filter quickly to specific test files

-t is similar to p but looks at the test title, i.e., the part that’s in quotes after it or describe

-q exits watch mode

-enter will trigger a re-run (useful, though the point of watch is that Jest will re-run any time there’s a meaningful change to files within scope of the current filters).