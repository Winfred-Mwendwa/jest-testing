import { TestBed } from '@angular/core/testing';
import { Hero } from './hero';
import { HeroService } from './hero.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';


describe('HeroService', () => {
  let httpTestingController: HttpTestingController;
  let service: HeroService;
  jest.setTimeout(6000);
 
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
 
    httpTestingController = TestBed.inject(HttpTestingController);
 
    service = TestBed.inject(HeroService);

    
  });
 
  afterEach(() => {
    httpTestingController.verify();
  });

  //test the getHeroes method
  it('#getData should return expected data', (done) => {
    const expectedData: Hero[] = [
      { id: 12, name: 'Dr. Nice' },
  { id: 13, name: 'Bombasto' },
  { id: 14, name: 'Celeritas' },
  { id: 15, name: 'Magneta' },
  { id: 16, name: 'RubberMan' },
  { id: 17, name: 'Dynama' },
  { id: 18, name: 'Dr. IQ' },
  { id: 19, name: 'Magma' },
  { id: 20, name: 'Tornado' }
    ];
 
    service.getHeroes().subscribe(data => {
      expect(data).toEqual(expectedData);
      done();
    });
 
    const testRequest = httpTestingController.expectOne('api/heroes');
 
    testRequest.flush(expectedData);
  });

  //test the getHero method
  it(' getHero by ID should return hero with requested ID ', (done) => {
    const expectedHero: Hero  = 
      { id: 12, name: 'Dr. Nice' }
    ;

    service.getHero(12).subscribe(data => {
      expect(data).toEqual(expectedHero);
      done();
    });

    const testRequest = httpTestingController.expectOne('api/heroes/12');
 
    testRequest.flush(expectedHero);
  });
   
  //test the deletehero method
  it('deleteHero should delete selected hero', (done) => {
    const deletedHero: Hero = {id: 12, name:'Dr. Nice'};
    
    
    service.deleteHero(12).subscribe(data => {
      expect(data).toEqual(deletedHero)
      done();
    });

    const testRequest = httpTestingController.expectOne('api/heroes/12');
 
    testRequest.flush(deletedHero);

  });

  //test the searchHeroes method
  it(' searchHero should return hero matching hero search term ', (done) => {
    const searchedHero: Hero []  =[
      { id: 12, name: 'Dr. Nice' },
  { id: 13, name: 'Bombasto' },
  { id: 14, name: 'Celeritas' },
  { id: 15, name: 'Magneta' },
  { id: 16, name: 'RubberMan' },
  { id: 17, name: 'Dynama' },
  { id: 18, name: 'Dr. IQ' },
  { id: 19, name: 'Magma' },
  { id: 20, name: 'Tornado' }
    ];

    service.searchHeroes("Dr. Nice").subscribe(data => {
      expect(data).toEqual(searchedHero);
      done();
    });

    const testRequest = httpTestingController.expectOne('api/heroes/?name=Dr. Nice');
 
    testRequest.flush(searchedHero);
    
  });

  //test the update hero method
  it(' updateHero should update hero ', (done) => {
    const oldHero: Hero  = 
      { id: 12, name: 'Dr. Nice' }
    ;

    const updatedHero: Hero = 
      { id: 12, name: 'Dr. Nicer'}

    service.updateHero(oldHero).subscribe(data => {
      expect(data).toEqual(updatedHero);
      done();
    });

    const testRequest = httpTestingController.expectOne('api/heroes');
 
    testRequest.flush(updatedHero);
  });

  //test the addHero method
  it(' addHero should add the hero', (done) => {
    const addedHero: Hero  = 
      { id: 21, name: 'Dr. Winnie' }
    ;

    service.addHero(addedHero).subscribe(data => {
      expect(data).toEqual(addedHero);
      done();
    });

    const testRequest = httpTestingController.expectOne('api/heroes');
 
    testRequest.flush(addedHero);
  });
 
})